import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiciByMarcaComponent } from './bici-by-marca.component';

describe('BiciByMarcaComponent', () => {
  let component: BiciByMarcaComponent;
  let fixture: ComponentFixture<BiciByMarcaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiciByMarcaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiciByMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
