import { MarcaService } from './../../../../marca/marca.service';
import { BiciclettaService } from './../../../../bicicletta/bicicletta.service';
import { Bicicletta } from './../../../../bicicletta/bicicletta';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Marca } from 'app/marca/marca';

@Component({
  selector: 'app-bici-by-marca',
  templateUrl: './bici-by-marca.component.html',
  styleUrls: ['./bici-by-marca.component.css']
})
export class BiciByMarcaComponent implements OnInit {

id: number
biciclette: Bicicletta[]
bikeBrand?: Marca

  constructor(private route: ActivatedRoute, 
    private biciclettaService: BiciclettaService, 
    private marcaService: MarcaService, 
    private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.biciclettaService.getByMarca(this.id).subscribe(data => {
      this.biciclette = data;
    })
    this.getMarca(this.id);
  }
  
  
  
    getMarca(id: number){
      this.id = this.route.snapshot.params['id'];
      this.marcaService.getById(this.id).subscribe(data => {
        this.bikeBrand = data;
      })
    }

  goToAllMarche(){
    this.router.navigate(['gestione_marche'])
  }

  getDettagli(id: number){
    this.router.navigate(['dettagli-bicicletta', id])
  }

  goToUpdate(id: number){
    this.router.navigate(['update-bicicletta', id])
  }

}
