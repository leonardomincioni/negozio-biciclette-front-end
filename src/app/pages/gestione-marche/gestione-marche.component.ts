import { Router } from '@angular/router';
import { MarcaService } from './../../marca/marca.service';
import { Marca } from './../../marca/marca';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-gestione-marche',
  templateUrl: './gestione-marche.component.html',
  styleUrls: ['./gestione-marche.component.css']
})
export class GestioneMarcheComponent implements OnInit {

  
  marche: Marca[];
  constructor(private marcaService: MarcaService,
    private router: Router) { }

  ngOnInit(): void {
    this.getAllMarche()
  }

  getAllMarche(){
    this.marcaService.getAllMarche().subscribe(
      (response: Marca[])=> {
        this.marche = response;
        console.log(this.marche)
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    );
  }

  getByMarca(id: number){
    this.router.navigate(['biciclette_by_marca', id])
  }

  addMarca(){
    this.router.navigate(['add-marca'])
  }

  goToUpdate(id: number){
    this.router.navigate(['update-marca', id])
  }
}
