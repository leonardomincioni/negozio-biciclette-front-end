import { MarcaService } from 'app/marca/marca.service';
import { Marca } from 'app/marca/marca';
import { catchError, map, delay } from 'rxjs/operators';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomValidationService {

marcheInserite : Marca[]

  constructor(private marcaService: MarcaService) { }

  validateBrandNotTaken(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> =>  {
      return this.brandTaken(control.value).pipe(
      map((taken) => (taken ? null: { brandTaken: true })),
      );
    };
  }
  

  brandTaken(brand: string): Observable<boolean>{
    return this.marcaService.getAllMarche().pipe(
      map((marcheInserite: Marca[]) =>
      marcheInserite.filter(marca => marca.brand.toLowerCase() === brand.toLowerCase())
      ),
      map(marche => !marche.length)
    );
    
  }
}
