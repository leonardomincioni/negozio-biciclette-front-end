import { CustomValidationService } from './validate-marca/custom-validation.service';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { Marca } from './../../../../marca/marca';
import { Component, OnInit } from '@angular/core';
import { MarcaService } from 'app/marca/marca.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-marca',
  templateUrl: './add-marca.component.html',
  styleUrls: ['./add-marca.component.css']
})
export class AddMarcaComponent implements OnInit {

  marca: Marca = {};
  // marcheInserite: Marca[]
  
  addMarcaForm: FormGroup;

  constructor(private marcaService: MarcaService,
    private customValidator: CustomValidationService,
    private router: Router) { }

  ngOnInit(): void {
    
    this.marca.isVisible=false;
    this.addMarcaForm = new FormGroup({
      brand: new FormControl('', {
        validators: [Validators.required], 
        asyncValidators: [this.customValidator.validateBrandNotTaken()],
        updateOn: 'blur'
      }),
      isVisible: new FormControl(null)
    });
}

  saveMarca(): void{


    this.marcaService.addMarca(this.marca).subscribe(data => {
      console.log(data);
      this.goToAllMarche();
    },
    error => console.log(error));
  }

  goToAllMarche(){
    this.router.navigate(['gestione_marche'])
  }

  onSubmit(){
    console.log(this.marca);
    this.saveMarca();
  }

 

}
