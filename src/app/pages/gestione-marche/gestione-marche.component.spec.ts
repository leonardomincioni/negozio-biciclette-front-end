import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneMarcheComponent } from './gestione-marche.component';

describe('GestioneMarcheComponent', () => {
  let component: GestioneMarcheComponent;
  let fixture: ComponentFixture<GestioneMarcheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestioneMarcheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneMarcheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
