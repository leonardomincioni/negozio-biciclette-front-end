import { BiciclettaService } from './../../../../bicicletta/bicicletta.service';
import { Bicicletta } from 'app/bicicletta/bicicletta';
import { CustomValidationService } from './../../add-marca/add-marca/validate-marca/custom-validation.service';
import { MarcaService } from 'app/marca/marca.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Marca } from 'app/marca/marca';
import { Component, OnInit } from '@angular/core';
import { data } from 'jquery';

@Component({
  selector: 'app-update-marca',
  templateUrl: './update-marca.component.html',
  styleUrls: ['./update-marca.component.css']
})
export class UpdateMarcaComponent implements OnInit {
  id:number
  marca: Marca = {}
  biciclette: Bicicletta[]
  updateMarcaForm: FormGroup


  constructor(private route: ActivatedRoute,
    private marcaService: MarcaService,
    private biciclettaService: BiciclettaService,
    private customValidator: CustomValidationService,
    private router: Router) { }

  ngOnInit(): void {
    this.id= this.route.snapshot.params['id'];

    this.biciclettaService.getByMarca(this.id).subscribe(data => {
      this.biciclette = data;
    })

    this.marcaService.getById(this.id).subscribe(data =>{
      this.marca=data;
    });

    this.updateMarcaForm = new FormGroup({
      brand: new FormControl('', {
        validators: [Validators.required], 
        asyncValidators: [this.customValidator.validateBrandNotTaken()],
        updateOn: 'blur'
      }),
      isVisible: new FormControl(null)
    });
  }

  onSubmit(){
    this.updateMarche(this.marca)
    this.backOnMarche()
  }

  backOnMarche(){
    this.router.navigate(['gestione_marche'])
  }

  updateMarche(marca: Marca){
    this.marcaService.update(marca).subscribe(data => {
      marca=data;
    })
  }

  deleteMarca(id: number){
    this.marcaService.delete(id).subscribe(data =>{
      this.marca;
      this.backOnMarche()
    })
  }

}
