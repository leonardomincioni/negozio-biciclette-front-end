import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuovoMovimentoComponent } from './nuovo-movimento.component';

describe('NuovoMovimentoComponent', () => {
  let component: NuovoMovimentoComponent;
  let fixture: ComponentFixture<NuovoMovimentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuovoMovimentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuovoMovimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
