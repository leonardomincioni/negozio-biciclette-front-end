import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MovementType } from './../../../movimento/movement-type.enum';
import { MovimentoService } from './../../../movimento/movimento.service';
import { Movimento } from 'app/movimento/movimento';
import { BiciclettaService } from './../../../bicicletta/bicicletta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Bicicletta } from './../../../bicicletta/bicicletta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nuovo-movimento',
  templateUrl: './nuovo-movimento.component.html',
  styleUrls: ['./nuovo-movimento.component.css']
})
export class NuovoMovimentoComponent implements OnInit {
  id: number;
  bicicletta: Bicicletta;
  movimento: Movimento = {};
  movimenti: Movimento[];
  tipoMovimento = MovementType;
  aggiungiForm: FormGroup;
  keys: string[]

  constructor(private route: ActivatedRoute,
    private biciclettaService: BiciclettaService,
    private movimentoService: MovimentoService,
    private router: Router) {

    this.id = this.route.snapshot.params['id'];
    this.keys = Object.keys(this.tipoMovimento);
  }

  ngOnInit(): void {

    this.aggiungiForm = new FormGroup({
      tipo: new FormControl(null, [Validators.required]),
      quantita: new FormControl(null, [Validators.required]),
    });


    this.keys = Object.keys(this.tipoMovimento);
    this.getBicicletta(this.id);
    this.getMovimentiByBicicletta(this.id);
  }

  //Recupera tutta la lista di movimenti associati alla bicicletta il cui id è passato in ingresso
  getMovimentiByBicicletta(id: number) {
    this.movimentoService.getMovimentiByBicicletta(id).subscribe(data => {
      this.movimenti = data;
    })
  }

  //Recupera la bicicletta a partire dall'id passato come parametro in ingresso
  getBicicletta(id: number): Bicicletta {
    this.biciclettaService.getById(id).subscribe(data => {
      this.bicicletta = data;
    })
    return this.bicicletta
  }

  sendMovimento(movimento: Movimento) {
    movimento.bicicletta = this.getBicicletta(this.id);
    this.movimentoService.sendMovimento(movimento).subscribe(data => {
      console.log(data);
      // this.router.navigate(['nuovo-movimento', this.id])
      this.getMovimentiByBicicletta(this.id)
      this.getBicicletta(this.id);


    },
      error => console.log(error));
  }

  backToDettagli(id: number) {
    this.router.navigate(['dettagli-bicicletta', id])
  }

  onSubmit() {
    console.log(this.movimento.tipoMovimento);
    this.sendMovimento(this.movimento);
    this.aggiungiForm.reset();

    // this.reload()
  }

  reload(){
    let currentUrl= this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl])
    });
  }

  validateForm() {
    this.aggiungiForm.markAsPristine();
    this.aggiungiForm.markAsUntouched();
    this.aggiungiForm.controls['tipo'].updateValueAndValidity();
    this.aggiungiForm.controls['quantita'].updateValueAndValidity();

    console.log(this.aggiungiForm.status);

    if (this.aggiungiForm.get('tipo').value == 'VENDITA') {
      this.aggiungiForm.controls['quantita'].setValidators([Validators.required, Validators.min(1), Validators.max(this.bicicletta.disponibilita)]);
      
    }
    if (this.aggiungiForm.get('tipo').value == 'RESTOCK') {
      this.aggiungiForm.controls['quantita'].setValidators([Validators.required, Validators.min(1)]);
      
    } 
    if (this.aggiungiForm.get('tipo').value == 'AGGIORNAMENTO') {
      this.aggiungiForm.controls['quantita'].setValidators([Validators.required, Validators.min(0)]);
      
    }
    this.aggiungiForm.controls['tipo'].updateValueAndValidity();
    this.aggiungiForm.controls['quantita'].updateValueAndValidity();
  }
}
    // if (this.movimento.tipoMovimento == MovementType.VENDITA) {
    //   this.aggiungiForm.clearValidators();
    //   this.aggiungiForm.setValidators({
    //     this.aggiungiForm.controls
    //     tipo: new FormControl(null, [Validators.required]),
    //     quantita: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(this.bicicletta.disponibilita)]),
    //   });
    //   console.log(this.movimento.tipoMovimento);
    // } else if (this.movimento.tipoMovimento == MovementType.RESTOCK) {
    //   this.aggiungiForm.clearValidators();
    //   this.aggiungiForm = new FormGroup({
    //     tipo: new FormControl(null, [Validators.required]),
    //     quantita: new FormControl(null, [Validators.required, Validators.min(1)]),
    //   });
    //   console.log(this.movimento.tipoMovimento);
    // } else if (this.movimento.tipoMovimento == MovementType.AGGIORNAMENTO) {
    //   this.aggiungiForm.clearValidators();
    //   this.aggiungiForm = new FormGroup({
    //     tipo: new FormControl(null, [Validators.required]),
    //     quantita: new FormControl(null, [Validators.required, Validators.min(0)]),
    //   });
    //   console.log(this.movimento.tipoMovimento);
    // }

 

