import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneMovimentiComponent } from './gestione-movimenti.component';

describe('GestioneMovimentiComponent', () => {
  let component: GestioneMovimentiComponent;
  let fixture: ComponentFixture<GestioneMovimentiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestioneMovimentiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneMovimentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
