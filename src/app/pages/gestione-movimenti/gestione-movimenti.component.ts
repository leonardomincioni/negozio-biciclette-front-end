import { HttpErrorResponse } from '@angular/common/http';
import { MovimentoService } from './../../movimento/movimento.service';
import { Component, OnInit } from '@angular/core';
import { Movimento } from 'app/movimento/movimento';

@Component({
  selector: 'app-gestione-movimenti',
  templateUrl: './gestione-movimenti.component.html',
  styleUrls: ['./gestione-movimenti.component.css']
})
export class GestioneMovimentiComponent implements OnInit {

  constructor(private movimentoService: MovimentoService) { }

  ngOnInit(): void {
    this.getMovimenti();
    
  }

  public movimenti: Movimento[];
  public movimento: Movimento;

  public getMovimenti(): void {
    this.movimentoService.getMovimenti().subscribe(
      (response: Movimento[]) => {
        this.movimenti = response;
        console.log(this.movimenti)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }
}
