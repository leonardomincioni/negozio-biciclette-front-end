import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneBiciComponent } from './gestione-bici.component';

describe('GestioneBiciComponent', () => {
  let component: GestioneBiciComponent;
  let fixture: ComponentFixture<GestioneBiciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestioneBiciComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneBiciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
