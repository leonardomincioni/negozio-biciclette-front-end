import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBiciclettaComponent } from './update-bicicletta.component';

describe('UpdateBiciclettaComponent', () => {
  let component: UpdateBiciclettaComponent;
  let fixture: ComponentFixture<UpdateBiciclettaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateBiciclettaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBiciclettaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
