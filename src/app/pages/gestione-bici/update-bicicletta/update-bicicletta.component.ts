import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Marca } from 'app/marca/marca';
import { MarcaService } from './../../../marca/marca.service';
import { BiciclettaService } from './../../../bicicletta/bicicletta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Bicicletta } from './../../../bicicletta/bicicletta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-bicicletta',
  templateUrl: './update-bicicletta.component.html',
  styleUrls: ['./update-bicicletta.component.css']
})
export class UpdateBiciclettaComponent implements OnInit {

  id: number;
  bicicletta: Bicicletta = {};
  marche: Marca[];
  updateBiciForm: FormGroup;

  constructor(private route: ActivatedRoute, 
    private biciclettaService: BiciclettaService, 
    private marcaService: MarcaService,
    private router: Router) { }


  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.bicicletta.marca ={}
    this.biciclettaService.getById(this.id).subscribe(data => {
      this.bicicletta = data;
    });
    this.marcaService.getAllMarche().subscribe(brands => {
      this.marche = brands;
    })

    this.updateBiciForm = new FormGroup({
      marca: new FormControl(null, Validators.required),
      modello: new FormControl(null, Validators.required),
      prezzo: new FormControl(null,  Validators.required),
      disponibilita: new FormControl({value:this.bicicletta.disponibilita, disabled: true}, Validators.required),
      isVisible: new FormControl(null, Validators.required)
    });
  }

  onSubmit(){
    console.log(this.bicicletta);
    this.updateBicicletta(this.bicicletta);
    this.backToHome();
  }

  backToHome(){
    this.router.navigate(['gestione-bici']);
  }

updateBicicletta(bicicletta: Bicicletta){
  this.biciclettaService.update(bicicletta).subscribe(data => {
    console.log(data);
  },
    error => console.log(error));
 }

 toNewMovimento(id:number){
  this.router.navigate(['nuovo-movimento', id]);
}
}
