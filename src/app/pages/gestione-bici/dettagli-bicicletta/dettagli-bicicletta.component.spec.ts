import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DettagliBiciclettaComponent } from './dettagli-bicicletta.component';

describe('DettagliBiciclettaComponent', () => {
  let component: DettagliBiciclettaComponent;
  let fixture: ComponentFixture<DettagliBiciclettaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DettagliBiciclettaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DettagliBiciclettaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
