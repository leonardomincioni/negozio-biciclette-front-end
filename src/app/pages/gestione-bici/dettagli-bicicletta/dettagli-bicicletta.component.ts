import { BiciclettaService } from './../../../bicicletta/bicicletta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Bicicletta } from 'app/bicicletta/bicicletta';

@Component({
  selector: 'app-dettagli-bicicletta',
  templateUrl: './dettagli-bicicletta.component.html',
  styleUrls: ['./dettagli-bicicletta.component.css']
})
export class DettagliBiciclettaComponent implements OnInit {

  id: number
  bicicletta: Bicicletta
  constructor(private route: ActivatedRoute, private biciclettaService: BiciclettaService, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.biciclettaService.getById(this.id).subscribe(data => {
      this.bicicletta = data;
    })
  }

  toHomePage():void{
    this.router.navigate(['gestione_bici'])
  }

  toNewMovimento(id:number):void{
    console.log(this.bicicletta.id)
    this.router.navigate(['nuovo-movimento', id])
  }
  
  goToUpdate(id: number){
    this.router.navigate(['update-bicicletta', id])
  }


}
