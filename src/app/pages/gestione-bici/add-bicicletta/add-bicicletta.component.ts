import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MarcaService } from './../../../marca/marca.service';
import { BiciclettaService } from './../../../bicicletta/bicicletta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Marca } from './../../../marca/marca';
import { Bicicletta } from './../../../bicicletta/bicicletta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-bicicletta',
  templateUrl: './add-bicicletta.component.html',
  styleUrls: ['./add-bicicletta.component.css']
})
export class AddBiciclettaComponent implements OnInit {

  id: number;
  bicicletta: Bicicletta = {};
  marche: Marca[];
  addBiciForm: FormGroup;

  constructor(private route: ActivatedRoute, 
    private biciclettaService: BiciclettaService, 
    private marcaService: MarcaService,
    private router: Router) { }


  ngOnInit(): void {
    this.bicicletta.marca = {}
    this.bicicletta.isVisible=false;
    this.marcaService.getAllMarche().subscribe(brands => {
      this.marche = brands;
    })

    this.addBiciForm = new FormGroup({
      marca: new FormControl(null, Validators.required),
      modello: new FormControl(null, Validators.required),
      prezzo: new FormControl(null,  Validators.required),
      disponibilita: new FormControl(null, Validators.required),
      isVisible: new FormControl(null, Validators.required)
    });
  }

  onSubmit(){
    console.log(this.bicicletta);
    this.saveBicicletta(this.bicicletta);
    this.backToHome();
  }

  backToHome(){
    this.router.navigate(['gestione-bici']);
  }

 saveBicicletta(bicicletta: Bicicletta){
  this.biciclettaService.insert(bicicletta).subscribe(data => {
    console.log(data);
  },
    error => console.log(error));
 }
}
