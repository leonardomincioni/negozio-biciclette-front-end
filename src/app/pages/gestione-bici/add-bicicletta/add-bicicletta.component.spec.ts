import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBiciclettaComponent } from './add-bicicletta.component';

describe('AddBiciclettaComponent', () => {
  let component: AddBiciclettaComponent;
  let fixture: ComponentFixture<AddBiciclettaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBiciclettaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBiciclettaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
