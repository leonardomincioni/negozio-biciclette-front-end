import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BiciclettaService } from './../../bicicletta/bicicletta.service';
import { Bicicletta } from './../../bicicletta/bicicletta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gestione-bici',
  templateUrl: './gestione-bici.component.html',
  styleUrls: ['./gestione-bici.component.css']
})
export class GestioneBiciComponent implements OnInit {


  biciclette: Bicicletta[];
  constructor(private biciclettaService: BiciclettaService, 
              private router: Router) { }

  ngOnInit(): void {
    this.getAllBiciclette()
  }

  getAllBiciclette(){
    this.biciclettaService.getBiciclette().subscribe(
      (response: Bicicletta[]) => {
        this.biciclette = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    );
  }


  getDettagli(id: number){
    this.router.navigate(['dettagli-bicicletta', id])
  }

  goToUpdate(id: number){
    this.router.navigate(['update-bicicletta', id])
  }

  toAddNew():void{
    this.router.navigate(['add-bicicletta'])
  }
}
