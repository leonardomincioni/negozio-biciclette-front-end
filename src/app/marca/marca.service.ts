import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Marca } from './marca';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getAllMarche(): Observable<Marca[]>{
    return this.http.get<any>(`${this.apiServerUrl}/brand`);
  }

  public getById(id: number):Observable<Marca>{
    return this.http.get<any>(`${this.apiServerUrl}/brand/${id}`);
  }

  public addMarca(marca: Marca): Observable<Marca>{
    return this.http.post<Marca>(`${this.apiServerUrl}/brand`, marca);
  }

  public update(marca: Marca): Observable<Marca>{
    return this.http.put<Marca>(`${this.apiServerUrl}/brand`, marca);
  }

  public delete(id: number): Observable<Object>{
    return this.http.delete(`${this.apiServerUrl}/brand/${id}`);
  }
}
