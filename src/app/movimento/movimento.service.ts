import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Movimento } from './movimento';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovimentoService {

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getMovimenti() : Observable<Movimento[]> {
    return this.http.get<Movimento[]>(`${this.apiServerUrl}/op`);
}

public getMovimentiByBicicletta(id:number) : Observable<Movimento[]> {
  return this.http.get<Movimento[]>(`${this.apiServerUrl}/op/${id}`);
}
public sendMovimento(movimento: Movimento) : Observable<Movimento> {
  return this.http.post<Movimento>(`${this.apiServerUrl}/op`, movimento);
}
}
