import { Bicicletta } from './../bicicletta/bicicletta';
import { MovementType } from './movement-type.enum';

export class Movimento {

    id?: number;
    timeStamp?: Date;
    quantita?: number;
    tipoMovimento?: MovementType;
    bicicletta?: Bicicletta;

}
