

import { GestioneMovimentiComponent } from './../../pages/gestione-movimenti/gestione-movimenti.component';
import { GestioneBiciComponent } from './../../pages/gestione-bici/gestione-bici.component';
import { GestioneMarcheComponent } from './../../pages/gestione-marche/gestione-marche.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    NgbModule,
   


  ],
  declarations: [
    GestioneMarcheComponent,
    GestioneBiciComponent,
    GestioneMovimentiComponent,
 
    
  ]
})

export class AdminLayoutModule {}
