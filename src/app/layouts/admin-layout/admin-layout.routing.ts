import { UpdateMarcaComponent } from './../../pages/gestione-marche/update-marca/update-marca/update-marca.component';
import { AddBiciclettaComponent } from './../../pages/gestione-bici/add-bicicletta/add-bicicletta.component';
import { UpdateBiciclettaComponent } from './../../pages/gestione-bici/update-bicicletta/update-bicicletta.component';
import { NuovoMovimentoComponent } from './../../pages/gestione-movimenti/nuovo-movimento/nuovo-movimento.component';
import { AddMarcaComponent } from './../../pages/gestione-marche/add-marca/add-marca/add-marca.component';
import { DettagliBiciclettaComponent } from './../../pages/gestione-bici/dettagli-bicicletta/dettagli-bicicletta.component';
import { BiciByMarcaComponent } from './../../pages/gestione-marche/bici-by-marca/bici-by-marca/bici-by-marca.component';
import { GestioneMovimentiComponent } from './../../pages/gestione-movimenti/gestione-movimenti.component';
import { GestioneBiciComponent } from './../../pages/gestione-bici/gestione-bici.component';
import { GestioneMarcheComponent } from './../../pages/gestione-marche/gestione-marche.component';
import { Routes } from '@angular/router';


export const AdminLayoutRoutes: Routes = [
    { path: 'gestione_marche', component: GestioneMarcheComponent},
    { path: 'gestione_bici', component: GestioneBiciComponent},
    { path: 'dettagli-bicicletta/:id', component: DettagliBiciclettaComponent },
    { path: 'gestione_movimenti', component: GestioneMovimentiComponent},
    { path: 'biciclette_by_marca/:id', component: BiciByMarcaComponent},
    { path: 'nuovo-movimento/:id', component: NuovoMovimentoComponent},
    { path: 'add-marca', component: AddMarcaComponent},
    { path: 'update-bicicletta/:id', component: UpdateBiciclettaComponent},
    { path: 'add-bicicletta', component: AddBiciclettaComponent},
    { path: 'update-marca/:id', component: UpdateMarcaComponent }
    
];
