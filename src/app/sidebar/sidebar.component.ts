import { Component, OnInit } from '@angular/core';


export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/gestione_marche', title: 'Gestione Marche',     icon:'nc-single-copy-04',    class: ''  },
    { path: '/gestione_bici',   title: 'Gestione Bici', icon:'nc-tag-content',  class: '' },
    { path: '/gestione_movimenti',   title: 'Gestione Movimenti', icon:'nc-shop',  class: '' }
    
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
}
