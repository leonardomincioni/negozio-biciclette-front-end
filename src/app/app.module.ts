import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from "ngx-toastr";

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { HttpClientModule } from "@angular/common/http";
import { BiciByMarcaComponent } from './pages/gestione-marche/bici-by-marca/bici-by-marca/bici-by-marca.component';
import { DettagliBiciclettaComponent } from './pages/gestione-bici/dettagli-bicicletta/dettagli-bicicletta.component';
import { AddMarcaComponent } from './pages/gestione-marche/add-marca/add-marca/add-marca.component';
import { NuovoMovimentoComponent } from './pages/gestione-movimenti/nuovo-movimento/nuovo-movimento.component';
import { UpdateBiciclettaComponent } from './pages/gestione-bici/update-bicicletta/update-bicicletta.component';
import { AddBiciclettaComponent } from './pages/gestione-bici/add-bicicletta/add-bicicletta.component';
import { UpdateMarcaComponent } from './pages/gestione-marche/update-marca/update-marca/update-marca.component';
import { DeleteMarcaComponent } from './pages/gestione-marche/update-marca/update-marca/delete-marca/delete-marca/delete-marca.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    BiciByMarcaComponent,
    DettagliBiciclettaComponent,
    AddMarcaComponent,
    NuovoMovimentoComponent,
    UpdateBiciclettaComponent,
    AddBiciclettaComponent,
    UpdateMarcaComponent,
    DeleteMarcaComponent
  ],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes,{
      useHash: false
    }),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
