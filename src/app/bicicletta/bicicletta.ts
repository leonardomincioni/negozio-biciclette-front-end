import { Marca } from './../marca/marca';

export class Bicicletta {
    id?: number;
    modello?: string;
    prezzo?: number;
    disponibilita?: number;
    marca?: Marca;
    isVisible?: boolean;
}
