import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bicicletta } from './bicicletta';

@Injectable({
  providedIn: 'root'
})
export class BiciclettaService {

  private apiServerUrl = environment.apiBaseUrl;


  constructor(private http: HttpClient) { }

  public getBiciclette(): Observable<Bicicletta[]>{
    return this.http.get<any>(`${this.apiServerUrl}/store`);
  }

  public getByMarca(id:number): Observable<Bicicletta[]>{
    return this.http.get<any>(`${this.apiServerUrl}/store/byBrand/${id}`);
  }

  public getById(id:number): Observable<Bicicletta>{
    return this.http.get<any>(`${this.apiServerUrl}/store/${id}`);
  }

  public insert(bicicletta:Bicicletta): Observable<Bicicletta>{
    return this.http.post<Bicicletta>(`${this.apiServerUrl}/store`, bicicletta);
  }

  public update(bicicletta:Bicicletta): Observable<Bicicletta>{
    return this.http.put<Bicicletta>(`${this.apiServerUrl}/store`, bicicletta);
  }
}
